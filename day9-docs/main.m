//
//  main.m
//  day9-docs
//
//  Created by Dave Henson on 11/22/17.
//  Copyright © 2017 Dave Henson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
